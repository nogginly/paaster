# PaaSter Changelog

The latest version of this file can be found at the master branch of the `nogginly/paaster` repository.

## 0.4.4

- Added `iam:DeleteRolePermissionsBoundary` to IAM policy for TF deployer.

## 0.4.3

- Fixed IAM policy for `paaster/aws/infra_tf_deployer` to be able to update policies after creating them.

## 0.4.2

- Modified `paaster/aws/infra_tfbe` module to 
    - fix typo in policy

## 0.4.1

- Modified `paaster/aws/infra_tfbe` module to 
    - clarify the S3 access policy
- Modified `paaster/aws/infra_tfbe_user` module to 
    - require KMS key used by TFBE for encryption
    - attach a user policy with access to the KMS key

## 0.4.0

- Modified `paaster/aws/infra_tf_deployer` module to 
    - only provision policies for enabled AWS servcies.
    - only provision the `DeployesCustom` group if custom policies are attached
    - add ReadCW support

## 0.3.1

- Adding S3 and DDB (Dynamo DB) support to `paaster/aws/infra_tf_deployer` module.

## 0.3.0

- Adding feature module
    - `paaster/aws/infra_tfbe_user` module to setup a dedicated TFBE IAM user

## 0.2.0

- Infrastructure provisioning separated into different feature modules:
    - `paaster/aws/infra_key` module to setup the infrastructure KMS key
    - `paaster/aws/infra_tfbe` module to setup resources for TF remote back-end using AWS
    - `paaster/aws/infra_cloudtrail` module to setup CloudTrail auditing
    - `paaster/aws/infra_tf_deployer` module to setup TF deployer IAM user and related groups and policies based on specified AWS services to enable
- Removed the "one big" module, which was just a working prototype until I could get the separate modules defined. 

## 0.1.0

- Initial version 
- Defines the `paaster/aws/infra` module that sets up the following for an AWS account with a single module import:
    - CloudTrail, with logging via CloudWatch Logs.
    - IAM user `tf_deploy` for application provisioning
    - IAM group `tf_deployers` associated with policies for provisioning KMS, ES, IAM, R53, APIG, Lambda, EC2.
    - Terraform remote back-end using S3/Dynamo DB, for applications provisioned using `terraform` to use.
    - Strict IAM password policy by default
- Implement the `bin/paasterraform` wrapper script that is account and `paaster` aware.

# PaaSter - Quick and easy PaaS account setup using Terraform

- [PaaSter - Quick and easy PaaS account setup using Terraform](#paaster---quick-and-easy-paas-account-setup-using-terraform)
    - [About](#about)
    - [Getting Started With Example](#getting-started-with-example)
        - [Pre-conditions](#pre-conditions)
        - [Getting Ready](#getting-ready)
        - [Initialize](#initialize)
        - [Check the plan](#check-the-plan)
        - [Apply the plan](#apply-the-plan)
        - [Cleanup (Undo) the plan](#cleanup-undo-the-plan)
    - [Getting Your Own Project Started](#getting-your-own-project-started)
        - [Install via `git` sub-modules](#install-via-git-sub-modules)
            - [Your `main.tf`](#your-maintf)
        - [Protecting Credentials](#protecting-credentials)
    - [Modules Reference](#modules-reference)
        - [`paaster/aws/infra_key` Module](#paasterawsinfrakey-module)
            - [Inputs](#inputs)
            - [Outputs](#outputs)
        - [`paaster/aws/infra_cloudtrail` Module](#paasterawsinfracloudtrail-module)
            - [Inputs](#inputs)
            - [Outputs](#outputs)
        - [`paaster/aws/infra_tfbe` Module](#paasterawsinfratfbe-module)
            - [Inputs](#inputs)
            - [Outputs](#outputs)
        - [`paaster/aws/infra_tfbe_user` Module](#paasterawsinfratfbeuser-module)
            - [Inputs](#inputs)
            - [Outputs](#outputs)
        - [`paaster/aws/infra_tf_deployer` Module](#paasterawsinfratfdeployer-module)
            - [Inputs](#inputs)
            - [Outputs](#outputs)
    - [CLI Reference](#cli-reference)
        - [`bin/paasterraform.sh`](#binpaasterraformsh)


## About

**PaaSter** is a set of `terraform` modules that make it faster to configure and deploy PaaS AWS cloud accounts with the essential systems enabled. This includes setting up CloudTrail, IAM policies, TF back-endm, etc with a few lines of Terraform configuration.

The purpose of `paaster` is to help automate the cloud-native platform setup. If you're using `faaster` to implement serverless micro-services, for example, then you can user `paaster` to first setup the platform/account for production vs staging as well as the TF provisioning credentials and then use that information with `faaster` to deploy the APIs. 

## Getting Started With Example 

### Pre-conditions

1. You need to have an AWS account set up.
2. You need an API key set up, either as a credential for your root account or by setting up an IAM user with admin access.
3. You need to have a GPG key pair set up; you will need the public key (binary, not armored) in base64 format.
4. You need the most recent `terraform` installed on your system. 
5. Clone the `nogginly/paaster` repository locally

### Getting Ready

1. Using your terminal/console `cd` into the `example/accounts/` folder.
2. Make a sub-folder called `myaccount`
3. Go into the `myaccount/` folder
4. Create a file called `credentials` and make sure it exports the following env variables:
    - `export AWS_ACCESS_KEY_ID=YOUR_ACCOUNT_ADMIN_ACCESS_KEY`
    - `export AWS_SECRET_ACCESS_KEY=YOUR_ACCOUNT_ADMIN_SECRET_KEY`
5. Copy your GPG public key file into this folder. Call it `myaccount_aws_myuser.gpg.pub.b64.asc`.
6. Create a file called `terraform.tfvars` and make sure it has the following properties:
    - `aws_region = YOUR_AWS_REGION_IF_NOT_US_EAST_1`
    - `deploy_name = "example"`
    - `gpg_publickey_file = "accounts/myaccount/myaccount_aws_myuser.gpg.pub.b64.asc`
7. Now `cd` back out so that you are back in the `example/` folder.

### Initialize

Run the following command to initialize your local account folder using the configuration.

```
$ ../bin/paasterraform myaccount init
```

### Check the plan

Run the following command to plan the configuration of your AWS account. 

```
$ ../bin/paasterraform myaccount plan
```

A lot of stuff will scroll by but eventually you should at least see a line like the following:

```
Plan: 36 to add, 0 to change, 0 to destroy.
```

### Apply the plan

Run the following command to apply the configuration to your AWS account. 

```
$ ../bin/paasterraform myaccount apply
```

You will be prompted to confirm by typing in `yes` and then you will need to wait as all the resources get provisioned. Some of the steps take a little while and it make take a few minutes to finish (because this is the first time).

### Cleanup (Undo) the plan

You can cleanup the example with the following command.

```
$ bin/paasterraform.sh myaccount destroy
```

## Getting Your Own Project Started 

### Install via `git` sub-modules

Define your infrastructure setup project folder structure as follows.

```
+-- myinfra/
    +-- accounts/                       <-- this folder will contain the AWS account sub-folders
    |   +-- ACC1/                       <-- folder for AWS account you want to setup using `paaster`
    |       +-- credentials             <-- The file with AWS account credentials as bash exports
    |       +-- acc1_gpg_pub_b64.asc    <-- The base-64 encoded binary (un-armored) GPG public key
    |       +-- terraform.tfvars        <-- Where your AWS account-specific TF config vars go.
    +-- deps/
    |   +-- paaster/                    <-- pull here as a submodule
    +-- modules/
    |   +-- paaster/                    <-- symlink to 'myapp/deps/paaster/modules`
    +-- main.tf                         <-- your main `terraform` config for infra setup using `paaster`
    +-- CHANGELOG.md
    +-- LICENSE
    +-- README.md
```

Using `git submodule` pull in the `paaster` repo under the `deps/` folder and then symlink the `deps/paaster/modules` as `modules/paaster` so that `paaster` modules show up in your projects `modules/` folder where `terraform` will look for modules that you use.

#### Your `main.tf`

See `example/main.tf`. You must setup the following three providers before using any of the `paaster` modules:

```tf
provider "aws" {
  region = "${var.aws_region}"
}

provider "random" {}

provider null {}
```

### Protecting Credentials

> When developing cloud-native applications using a `git` repo, it's important to make sure you don't check-in credentials and other important stuff into a public repo somewhere. Take a look at the `.gitignore` file in this repo and feel free to use it as a starting point. It will keep the usual places where creds are put as well as runtime data files etc out of your app's repo.


## Modules Reference

### `paaster/aws/infra_key` Module

#### Inputs

| Name            | Description                                                                                                                                        | Type   | Default | Required |
| --------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| deletion_window | Optional duration in days (between 7 and 30 inclusive) after which key is deleted after destruction of this module's resources. Default is 10 days | string | `10`    | no       |
| name            | A short name for the key, it will be used as a prefix for the KMS key name.                                                                        | string | -       | yes      |

#### Outputs

| Name    | Description                                                             |
| ------- | ----------------------------------------------------------------------- |
| key_arn | The ARN for infrastructure-specific KMS key provisioned by this module. |

### `paaster/aws/infra_cloudtrail` Module

#### Inputs

| Name    | Description                                                                                                                         | Type   | Default | Required |
| ------- | ----------------------------------------------------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| key_arn | ARN for the KMS key to use to encrypt the data managed by the resources created by this module. Encryption at rest is not optional. | string | -       | yes      |
| name    | A short nameto be used as prefix where possible for resources created by this module. Keep it short and use following pattern:      | string | -       | yes      |

#### Outputs

None.

### `paaster/aws/infra_tfbe` Module

#### Inputs

| Name           | Description                                                                                                                                                      | Type   | Default | Required |
| -------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :-----: | :------: |
| dydb_read_cap  | Optional read capacity for the Dynamo DB table associated with this module. Defaults to 5.                                                                       | string | `5`     | no       |
| dydb_write_cap | Optional write capacity for the Dynamo DB table associated with this module. Defaults to 5.                                                                      | string | `5`     | no       |
| key_arn        | ARN for the KMS key to use to encrypt the data managed by the resources created by this module. Encryption at rest is not optional.                              | string | -       | yes      |
| name           | A short name for the terraform back-end, it will be used as prefix where possible for resources created by this module. Keep it short and use following pattern: | string | -       | yes      |

#### Outputs

| Name              | Description                                                                                                             |
| ----------------- | ----------------------------------------------------------------------------------------------------------------------- |
| tfbe_dynamo_table | The name of the dynamo table to use when configuring application's TF config to use an AWS S3-based TF remote back-end. |
| tfbe_policy_arn   | The IAM policy ARN that should assigned to IAM user who will be using the TFBE resources.                               |
| tfbe_s3_bucket    | The name of the S3 bucket to use when configuring application's TF config to use an AWS S3-based TF remote back-end.    |

### `paaster/aws/infra_tfbe_user` Module

#### Inputs

| Name               | Description                                                                                                                    | Type   | Default | Required |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ | :----: | :-----: | :------: |
| gpg_publickey_file | Path to the base64 encoded GPG public key file used to encrypt the AWS access and secret key for the deployment user.          | string | -       | yes      |
| key_arn            | ARN for the KMS key to allow the user to use when accessing encrypted resources.                                               | string | -       | yes      |
| name               | A short nameto be used as prefix where possible for resources created by this module. Keep it short and use following pattern: | string | -       | yes      |
| tfbe_policy_arn    | The IAM policy ARN that allows this user to access/use the TFBE resources.                                                     | string | -       | yes      |
| user_suffix        | An optional short name to be used as suffix to the IAM user created by this module. Default is "user".                         | string | `user`  | no       |

#### Outputs

| Name                           | Description                                                                                                                                               |
| ------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------- |
| tfbe_user_accesskey            | The AWS API access key for the TFBE user, to be used when configuring remote backend for application config using terraform.                              |
| tfbe_user_deploy_enc_secretkey | The GPG-encrypted base64-encoded AWS API secret key for the TFBE user, to be used when configuring remote backend for application config using terraform. |

### `paaster/aws/infra_tf_deployer` Module

#### Inputs

| Name                  | Description                                                                                                                                                                                                                                       | Type   | Default  | Required |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: | :------: | :------: |
| attach_policies       | Optional list of custom AWS policy ARN's to attach to the deployment user. Default is empty list.                                                                                                                                                 | list   | `<list>` | no       |
| attach_policies_count | Optional. Number of custom AWS policy ARN's to attach to the deployment user; should be number of elements in the `attach_policies` list property. Default is zero.                                                                               | string | `0`      | no       |
| enable_aws_services   | A map of one or more AWS services to which full (or limited if name specifies it) access is enabled for the TF deployer user/group. Default is no access. Supported service keys are: S3, DDB, KMS, EC2, APIG, Lambda, ReadACM, ES, CF, R53, IAM. | map    | `<map>`  | no       |
| gpg_publickey_file    | Path to the base64 encoded GPG public key file used to encrypt the AWS access and secret key for the deployment user.                                                                                                                             | string | -        | yes      |
| infra_key_arn         | ARN for the infrastructure KMS key to exclude from IAM KMS access for deployer. Required if enabling KMS service.                                                                                                                                 | string | `""`     | no       |
| name                  | A short nameto be used as prefix where possible for resources created by this module. Keep it short and use following pattern:                                                                                                                    | string | -        | yes      |

#### Outputs

| Name                      | Description                                                                                                                                      |
| ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| tf_deployer_accesskey     | The AWS API access key for the TF deployment user created by this module, to be used when provisioning application.                              |
| tf_deployer_enc_secretkey | The GPG-encrypted base64-encoded AWS API secret key for the TF deployment user created by this module, to be used when provisioning application. |


## CLI Reference

### `bin/paasterraform.sh`

`paasterraform.sh` is the account-aware `paaster`-specific wrapper script arround the `terraform` command ... 

> TODO: More details, coming soon.

```
Usage: ../bin/paasterraform.sh TARGET TFCMD [TFOPTIONS...]

    TARGET - name of configuration target in your / folder
             with the 'credentials' file used to source in env vars. Required.
    TFCMD - The command for 'terraform' followed by its options etc.
```

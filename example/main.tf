// Bring in the AWS provider
provider "aws" {
  region = "${var.aws_region}"
}

// Bring in the `random` provider
provider "random" {}

// Bring in the `null` provider
provider null {}

module "example_infra_key" {
  source          = "./modules/paaster/aws/infra_key"
  name            = "${var.deploy_name}"
  deletion_window = 7
}

module "example_infra_tfbe" {
  source  = "./modules/paaster/aws/infra_tfbe"
  name    = "${var.deploy_name}"
  key_arn = "${module.example_infra_key.key_arn}"
}

module "example_infra_tfbe_user" {
  source             = "./modules/paaster/aws/infra_tfbe_user"
  name               = "${var.deploy_name}"
  key_arn            = "${module.example_infra_key.key_arn}"
  gpg_publickey_file = "${var.gpg_publickey_file}"
  tfbe_policy_arn    = "${module.example_infra_tfbe.tfbe_policy_arn}"
}

module "example_infra_cloudtrail" {
  source  = "./modules/paaster/aws/infra_cloudtrail"
  name    = "${var.deploy_name}"
  key_arn = "${module.example_infra_key.key_arn}"
}

module "example_infra_tf_deployer" {
  source             = "./modules/paaster/aws/infra_tf_deployer"
  name               = "${var.deploy_name}"
  infra_key_arn      = "${module.example_infra_key.key_arn}"
  gpg_publickey_file = "${var.gpg_publickey_file}"

  enable_aws_services = {
    KMS    = "true"
    Lambda = "true"
    APIG   = "true"
    S3     = "true"
    DDB    = "true"
  }

  attach_policies = [
    "${module.example_infra_tfbe.tfbe_policy_arn}",
  ]

  attach_policies_count = 1
}

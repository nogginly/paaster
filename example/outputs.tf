output "tfbe" {
  description = "TFBE properties"

  value = {
    dynamo_table = "${module.example_infra_tfbe.tfbe_dynamo_table}"
    s3_bucket    = "${module.example_infra_tfbe.tfbe_s3_bucket}"
  }
}

output "tfbe_user" {
  description = "TFBE user properties"

  value = {
    access_key     = "${module.example_infra_tfbe_user.tfbe_user_accesskey}"
    enc_secret_key = "${module.example_infra_tfbe_user.tfbe_user_deploy_enc_secretkey}"
  }
}

output "tf_deployer" {
  description = "TF deployer user properties"

  value = {
    access_key     = "${module.example_infra_tf_deployer.tf_deployer_accesskey}"
    enc_secret_key = "${module.example_infra_tf_deployer.tf_deployer_enc_secretkey}"
  }
}

variable "aws_region" {
  description = "AWS region to use when setting up infrastructure resources that are region specific. Defaults to `us-east-1`."
  default     = "us-east-1"
}

variable "deploy_name" {
  description = "Short name with letters, numerals and dashes. Defaults to `example`."
  default     = "example"
}

variable "gpg_publickey_file" {
  description = "GPG Public key file with which to encrypt secrets like AWS access keys."
}

# Automatically provisioned by `tf_acctdeploy.sh`, use it when specifying files within the account configuration folder (e.g. GPG public key)
variable "config_path" {
  description = "This is the relative path to the AWS account configuration, and its value is set automatically when using the `bin/tf_acctdeploy` script to run terraform."
}

// Cloud Trail for paaster
resource "aws_cloudtrail" "paaster" {
  name                          = "${var.name}-trail"
  s3_bucket_name                = "${aws_s3_bucket.paaster-cloudtrail.id}"
  s3_key_prefix                 = "${var.name}-"
  include_global_service_events = true
  kms_key_id                    = "${var.key_arn}"
  enable_log_file_validation    = true
  is_multi_region_trail         = false

  cloud_watch_logs_group_arn = "${aws_cloudwatch_log_group.paaster-cloudtrail.arn}"
  cloud_watch_logs_role_arn  = "${aws_iam_role.paaster-ct-logs.arn}"

  // This dependency is needed to ensure we have a proper delay
  // to deal with dependency, otherwise we get a weird error.
  depends_on = ["null_resource.waiting_for_cloudwatch_logs_role"]
}

// Unique part to the S3 bucket identifier
resource "random_id" "paaster-ct-s3" {
  byte_length = 12
  prefix      = "${var.name}-ct-"
}

// S3 bucket for CT logs.
resource "aws_s3_bucket" "paaster-cloudtrail" {
  bucket        = "${random_id.paaster-ct-s3.dec}"
  force_destroy = true
  acl           = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "${var.key_arn}"
      }
    }
  }

  lifecycle_rule {
    id      = "paaster-ct-logs"
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 180
      storage_class = "GLACIER"
    }
  }
}

resource "aws_s3_bucket_policy" "paaster-cloudtrail" {
  bucket = "${aws_s3_bucket.paaster-cloudtrail.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "${aws_s3_bucket.paaster-cloudtrail.arn}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.paaster-cloudtrail.arn}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}

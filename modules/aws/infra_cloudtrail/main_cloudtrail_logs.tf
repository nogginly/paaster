resource "aws_cloudwatch_log_group" "paaster-cloudtrail" {
  name       = "CloudTrailLogs-${var.name}"
  kms_key_id = "${var.key_arn}"
}

// IAM Role that enables CWL access for CT
resource "aws_iam_role" "paaster-ct-logs" {
  name = "Logger-${var.name}-CloudTrail"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "paaster-ct-logs" {
  name = "LogTo-${var.name}-CloudTrail"
  role = "${aws_iam_role.paaster-ct-logs.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {

      "Sid": "AWSCloudTrailCreateLogStream2014110",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "${aws_cloudwatch_log_group.paaster-cloudtrail.arn}"
      ]

    }
  ]
}
EOF
}

// We need this to prevent the cloud train creation from timing out too early
// The Cloud Trail depends on this so that there is a long enough wait before it is
// created to ensure role policies are ready.
resource "null_resource" "waiting_for_cloudwatch_logs_role" {
  triggers {
    policy_id = "${aws_iam_role_policy.paaster-ct-logs.id}"
    role_arn  = "${aws_iam_role.paaster-ct-logs.arn}"
  }

  # Sleep 5 minutes to finish role creation completely
  provisioner "local-exec" {
    command = "sleep 300"
  }
}

variable "name" {
  description = "A short nameto be used as prefix where possible for resources created by this module. Keep it short and use following pattern: " # [a-z\]+\[a-z0-9\\-\]+`"
}

variable "key_arn" {
  description = "ARN for the KMS key to use to encrypt the data managed by the resources created by this module. Encryption at rest is not optional."
}

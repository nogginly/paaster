// Region info
data "aws_region" "current" {}

// Account and user info
data "aws_caller_identity" "current" {}

// The KMS key associated with AWS infrastructure that we control, accessible by
//  - Root account
//  - CloudWatch Logs
//  - S3 buckets
//  - CloudTrail
resource "aws_kms_key" "infra" {
  description             = "${var.name} Infra Key"
  deletion_window_in_days = "${var.deletion_window}"
  enable_key_rotation     = "true"

  policy = <<EOF
{
  "Version" : "2012-10-17",
  "Statement" : [
    {
      "Sid" : "Enable IAM User Permissions",
      "Effect" : "Allow",
      "Principal" : {
        "AWS" : [
          "${data.aws_caller_identity.current.arn}",
          "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
        ]
      },
      "Action" : "kms:*",
      "Resource" : "*"
    },
    {
      "Effect": "Allow",
      "Principal": { "Service": [
        "logs.${data.aws_region.current.name}.amazonaws.com", 
        "s3.${data.aws_region.current.name}.amazonaws.com" 
      ]},
      "Action": [ 
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*"
    },  
    {
      "Sid": "Allow CloudTrail to encrypt logs",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": [ 
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*",
      "Condition": {
        "StringLike": {
          "kms:EncryptionContext:aws:cloudtrail:arn": "arn:aws:cloudtrail:*:${data.aws_caller_identity.current.account_id}:trail/*"
        }
      }
    },
    {
      "Sid": "Allow CloudTrail to describe key",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "kms:DescribeKey",
      "Resource": "*"
    }
  ]
}
EOF
}

output "key_arn" {
  description = "The ARN for infrastructure-specific KMS key provisioned by this module."
  value       = "${aws_kms_key.infra.arn}"
}

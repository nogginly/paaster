// This defines the IAM group to which the `tf_deploy` belongs.
// The group pulls in the policies that allows for terraform-based application deployment, with support for
//  - S3
//  - DynamoDB
//  - Certidificate Manager (ACM)
//  - Lambda
//  - API Gateway
//  - EC2
//  - Route53
//  - CloudFront
//  - IAM
//  - KMS
//  - Back-end for TFState backups

resource "aws_iam_group" "tf_deployers" {
  name = "TF-${var.name}-Deployers"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_S3" {
  count      = "${lookup(var.enable_aws_services, "S3", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${data.aws_iam_policy.awsFullS3.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_DDB" {
  count      = "${lookup(var.enable_aws_services, "DDB", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${data.aws_iam_policy.awsFullDDB.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_EC2" {
  count      = "${lookup(var.enable_aws_services, "EC2", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${data.aws_iam_policy.awsFullEC2.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_Lambda" {
  count      = "${lookup(var.enable_aws_services, "Lambda", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${data.aws_iam_policy.awsFullLambda.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_APIG" {
  count      = "${lookup(var.enable_aws_services, "APIG", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${data.aws_iam_policy.awsFullAPIG.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_ReadACM" {
  count      = "${lookup(var.enable_aws_services, "ReadACM", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${data.aws_iam_policy.awsReadACM.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_ReadCW" {
  count      = "${lookup(var.enable_aws_services, "ReadCW", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${aws_iam_policy.deployReadCW.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_R53" {
  count      = "${lookup(var.enable_aws_services, "R53", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${aws_iam_policy.deployR53.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_CF" {
  count      = "${lookup(var.enable_aws_services, "CF", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${aws_iam_policy.deployCF.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_IAM" {
  count      = "${lookup(var.enable_aws_services, "IAM", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${aws_iam_policy.deployIAM.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_KMS" {
  count      = "${lookup(var.enable_aws_services, "KMS", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${aws_iam_policy.deployKMS.arn}"
}

resource "aws_iam_group_policy_attachment" "tf_deploy_ES" {
  count      = "${lookup(var.enable_aws_services, "ES", false) == true ? 1 : 0}"
  group      = "${aws_iam_group.tf_deployers.name}"
  policy_arn = "${aws_iam_policy.deployES.arn}"
}

// TF deployer user belong to the "Deployers" group
resource "aws_iam_group_membership" "tf_deployers" {
  name  = "tf-${var.name}-deployers-grp-memb"
  group = "${aws_iam_group.tf_deployers.name}"

  users = [
    "${aws_iam_user.tf_deploy.name}",
  ]
}

// "DeployersCustom" group to which custom policies are attached
// This group is inlo created if custom policies are attached.

resource "aws_iam_group" "tf_deployers_cust_policies" {
  count = "${var.attach_policies_count > 0 ? 1 : 0}"
  name  = "TF-${var.name}-DeployersCustom"
}

// Attach each of the caller specified policy ARNs to the "DeployersCustom" group
resource "aws_iam_group_policy_attachment" "tf_deploy_cust_policies" {
  count      = "${var.attach_policies_count}"
  group      = "${aws_iam_group.tf_deployers_cust_policies.name}"
  policy_arn = "${element(var.attach_policies, count.index)}"
}

// TF deployer user belong to the "DeployersCustom" group
resource "aws_iam_group_membership" "tf_deployers_cust_policies" {
  count = "${var.attach_policies_count > 0 ? 1 : 0}"
  name  = "tf-${var.name}-deployers-custom-grp-memb"
  group = "${aws_iam_group.tf_deployers_cust_policies.name}"

  users = [
    "${aws_iam_user.tf_deploy.name}",
  ]
}

resource "aws_iam_policy" "deployReadCW" {
  count       = "${lookup(var.enable_aws_services, "ReadCW", false) == true ? 1 : 0}"
  description = "CloudWatch read-only access policy for Terraform deployers"
  name        = "TF_${var.name}_DeployerCWAccess"

  policy = <<END
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "autoscaling:Describe*",
                "cloudwatch:Describe*",
                "cloudwatch:Get*",
                "cloudwatch:List*",
                "logs:Get*",
                "logs:List*",
                "logs:Describe*",
                "logs:TestMetricFilter",
                "sns:Get*",
                "sns:List*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}
END
}

data "aws_iam_policy" "awsReadACM" {
  arn = "arn:aws:iam::aws:policy/AWSCertificateManagerReadOnly"
}

data "aws_iam_policy" "awsFullLambda" {
  arn = "arn:aws:iam::aws:policy/AWSLambdaFullAccess"
}

data "aws_iam_policy" "awsFullS3" {
  arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

data "aws_iam_policy" "awsFullDDB" {
  arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

data "aws_iam_policy" "awsFullAPIG" {
  arn = "arn:aws:iam::aws:policy/AmazonAPIGatewayAdministrator"
}

data "aws_iam_policy" "awsFullEC2" {
  arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_policy" "deployR53" {
  count       = "${lookup(var.enable_aws_services, "R53", false) == true ? 1 : 0}"
  description = "Route53 access policy for Terraform deployers"
  name        = "TF_${var.name}_DeployerR53Access"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "route53:ListTrafficPolicyInstances",
                "route53:GetTrafficPolicyInstanceCount",
                "route53:GetChange",
                "route53:ListTrafficPolicyVersions",
                "route53:TestDNSAnswer",
                "route53:GetHostedZone",
                "route53:GetHealthCheck",
                "route53:ListHostedZonesByName",
                "route53:GetCheckerIpRanges",
                "route53:ListTrafficPolicies",
                "route53:ListResourceRecordSets",
                "route53:ListGeoLocations",
                "route53:GetTrafficPolicyInstance",
                "route53:GetHostedZoneCount",
                "route53:GetHealthCheckCount",
                "route53:ListReusableDelegationSets",
                "route53:GetHealthCheckLastFailureReason",
                "route53:GetHealthCheckStatus",
                "route53:ListHostedZones",
                "route53:ListTrafficPolicyInstancesByHostedZone",
                "route53:ChangeResourceRecordSets",
                "route53:GetReusableDelegationSet",
                "route53:ListTagsForResource",
                "route53:ListTagsForResources",
                "route53:ListTrafficPolicyInstancesByPolicy",
                "route53:ListHealthChecks",
                "route53:GetGeoLocation",
                "route53:GetTrafficPolicy"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "deployKMS" {
  count       = "${lookup(var.enable_aws_services, "KMS", false) == true ? 1 : 0}"
  name        = "TF_${var.name}_DeployerKMSAccess"
  description = "Policy to provide KMS access for terraform deployment."

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "AllowNewKeys",
          "Effect": "Allow",
          "Action": [
              "kms:ListKeys",
              "kms:ListAliases",
              "kms:CreateAlias",
              "kms:CreateKey"
          ],
          "Resource": "*"
      },
      {
          "Sid": "AllowKeyChangesExclInfraKey",
          "Effect": "Allow",
          "Action": [
              "kms:EnableKeyRotation",
              "kms:EnableKey",
              "kms:ListKeyPolicies",
              "kms:UntagResource",
              "kms:UpdateKeyDescription",
              "kms:PutKeyPolicy",
              "kms:GetKeyPolicy",
              "kms:ListResourceTags",
              "kms:DisableKeyRotation",
              "kms:TagResource",
              "kms:GetKeyRotationStatus",
              "kms:ScheduleKeyDeletion",
              "kms:DescribeKey",
              "kms:DeleteAlias"
          ],
          "NotResource": "${var.infra_key_arn}"
      }
    ]
}
EOF
}

resource "aws_iam_policy" "deployCF" {
  count       = "${lookup(var.enable_aws_services, "CF", false) == true ? 1 : 0}"
  name        = "TF_${var.name}_DeployerCFAccess"
  description = "CloudFront access policy for Terraform deployers."

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "cloudfront:UpdateDistribution",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "deployES" {
  count       = "${lookup(var.enable_aws_services, "ES", false) == true ? 1 : 0}"
  name        = "TF_${var.name}_DeployerESAccess"
  description = "ElasticSearch access for TF deployer"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "es:AddTags",
                "es:RemoveTags",
                "es:ListDomainNames",
                "es:DescribeElasticsearchDomain",
                "es:DeleteElasticsearchDomain",
                "es:UpdateElasticsearchDomainConfig",
                "es:CreateElasticsearchDomain",
                "es:ListTags",
                "es:DescribeElasticsearchDomainConfig",
                "es:DescribeElasticsearchDomains"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "deployIAM" {
  count       = "${lookup(var.enable_aws_services, "IAM", false) == true ? 1 : 0}"
  name        = "TF_${var.name}_DeployerIAMAccess"
  description = "IAM access policy for Terraform deployers."

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "iam:DeletePolicy",
                "iam:CreateRole",
                "iam:AttachRolePolicy",
                "iam:PutRolePolicy",
                "iam:GetGroup",
                "iam:DetachRolePolicy",
                "iam:DeleteRolePermissionsBoundary",
                "iam:DeleteRolePolicy",
                "iam:ListAttachedRolePolicies",
                "iam:ListRolePolicies",
                "iam:PutGroupPolicy",
                "iam:ListPolicies",
                "iam:GetRole",
                "iam:GetPolicy",
                "iam:ListGroupPolicies",
                "iam:ListRoles",
                "iam:DeleteRole",
                "iam:UpdateRoleDescription",
                "iam:ListUserPolicies",
                "iam:CreatePolicy",
                "iam:CreatePolicyVersion",
                "iam:ListPolicyVersions",
                "iam:GetUserPolicy",
                "iam:ListGroupsForUser",
                "iam:PutUserPolicy",
                "iam:GetGroupPolicy",
                "iam:GetUser",
                "iam:GetRolePolicy",
                "iam:DeletePolicyVersion",
                "iam:ListInstanceProfilesForRole"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

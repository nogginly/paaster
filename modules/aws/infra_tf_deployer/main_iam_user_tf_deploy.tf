//
// Setup the IAM user credentials used for terraform application deployment.
resource "aws_iam_user" "tf_deploy" {
  name          = "tf_${var.name}_deploy"
  force_destroy = true
}

// Load up the GPG public key file needed to properly setup IAM users
data "local_file" "b64publickey" {
  filename = "${var.gpg_publickey_file}"
}

resource "aws_iam_access_key" "tf_deploy" {
  user    = "${aws_iam_user.tf_deploy.name}"
  pgp_key = "${data.local_file.b64publickey.content}"
}

output "tf_deployer_accesskey" {
  description = "The AWS API access key for the TF deployment user created by this module, to be used when provisioning application."
  value       = "${aws_iam_access_key.tf_deploy.id}"
}

output "tf_deployer_enc_secretkey" {
  description = "The GPG-encrypted base64-encoded AWS API secret key for the TF deployment user created by this module, to be used when provisioning application."
  value       = "${aws_iam_access_key.tf_deploy.encrypted_secret}"
}

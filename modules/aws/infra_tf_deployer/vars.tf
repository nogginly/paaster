variable "name" {
  description = "A short nameto be used as prefix where possible for resources created by this module. Keep it short and use following pattern: " # [a-z\]+\[a-z0-9\\-\]+`"
}

variable "infra_key_arn" {
  description = "ARN for the infrastructure KMS key to exclude from IAM KMS access for deployer. Required if enabling KMS service."
  type        = "string"
  default     = ""
}

variable "gpg_publickey_file" {
  description = "Path to the base64 encoded GPG public key file used to encrypt the AWS access and secret key for the deployment user."
}

variable "enable_aws_services" {
  description = "A map of one or more AWS services to which full (or limited if name specifies it) access is enabled for the TF deployer user/group. Default is no access. Supported service keys are: S3, DDB, KMS, EC2, APIG, Lambda, ReadACM, ReadCW, ES, CF, R53, IAM."
  type        = "map"

  default = {
    // no access
  }
}

variable "attach_policies" {
  description = "Optional list of custom AWS policy ARN's to attach to the deployment user. Default is empty list."
  type        = "list"
  default     = []
}

variable "attach_policies_count" {
  description = "Optional. Number of custom AWS policy ARN's to attach to the deployment user; should be number of elements in the `attach_policies` list property. Default is zero."
  default     = 0
}

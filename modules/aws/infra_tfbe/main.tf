// Setup a bucket and table for enabling Terraform state backup.
// Unique part to the S3 bucket identifier
resource "random_id" "paaster-tf-backend-s3" {
  byte_length = 12
  prefix      = "${var.name}-tfbe-"
}

// Encrypted bucket for TF backend state management
resource "aws_s3_bucket" "paaster-tf-backend" {
  bucket        = "${random_id.paaster-tf-backend-s3.dec}"
  force_destroy = false                                    // don't force destroy.
  acl           = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "${var.key_arn}"
      }
    }
  }

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "${var.name}-tfbe-lifecycle"
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }
  }
}

// Unique part to the DynamoDB table identifier
resource "random_id" "paaster-tf-backend-dydb" {
  byte_length = 12
  prefix      = "${var.name}-tfbe-"
}

// DynamoDB table for TF backend lock management
resource "aws_dynamodb_table" "paaster-tf-backend" {
  name           = "${random_id.paaster-tf-backend-dydb.dec}"
  read_capacity  = "${var.dydb_read_cap}"
  write_capacity = "${var.dydb_write_cap}"
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  server_side_encryption {
    enabled = true
  }
}

resource "aws_iam_policy" "backendForTF" {
  name        = "TF_${var.name}_BackendStateAccess"
  description = "S3 and Dynamod DB access for TF backend remote state/locking management"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Sid": "AllowAccessToTFBackendState",
      "Action": [
        "s3:ListBucket",
        "s3:GetBucketAcl"
      ],
      "Resource": "${aws_s3_bucket.paaster-tf-backend.arn}"
    },
    {
      "Effect": "Allow",
      "Sid": "AllowChangesToTFBackendState",
      "Action": [
        "s3:GetObject", 
        "s3:GetObjectAcl", 
        "s3:PutObjectAcl"
      ],
      "Resource": "${aws_s3_bucket.paaster-tf-backend.arn}/*"
    },
    {
      "Effect": "Allow",
      "Sid": "AllowWriteToTFBackendState",
      "Action": [
        "s3:PutObject"
      ],
      "Resource": "${aws_s3_bucket.paaster-tf-backend.arn}/*",
      "Condition":{
        "StringEquals":{
            "s3:x-amz-server-side-encryption-aws-kms-key-id": "${var.key_arn}",
            "s3:x-amz-server-side-encryption":"aws:kms"
        }
      }
    },
    {
      "Effect": "Allow",
      "Sid": "AllowAccessToTFBackendStateLocking",
      "Action": [
        "dynamodb:PutItem", 
        "dynamodb:UpdateItem",
        "dynamodb:DeleteItem",
        "dynamodb:GetItem"
      ],
      "Resource": "${aws_dynamodb_table.paaster-tf-backend.arn}"
    }
  ]
}
EOF
}

output "tfbe_dynamo_table" {
  description = "The name of the dynamo table to use when configuring application's TF config to use an AWS S3-based TF remote back-end."
  value       = "${aws_dynamodb_table.paaster-tf-backend.name}"
}

output "tfbe_s3_bucket" {
  description = "The name of the S3 bucket to use when configuring application's TF config to use an AWS S3-based TF remote back-end."
  value       = "${aws_s3_bucket.paaster-tf-backend.bucket}"
}

output "tfbe_policy_arn" {
  description = "The IAM policy ARN that should assigned to IAM user who will be using the TFBE resources."
  value       = "${aws_iam_policy.backendForTF.arn}"
}

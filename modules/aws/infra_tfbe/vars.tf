variable "name" {
  description = "A short name for the terraform back-end, it will be used as prefix where possible for resources created by this module. Keep it short and use following pattern: " # [a-z\]+\[a-z0-9\\-\]+`"
}

variable "key_arn" {
  description = "ARN for the KMS key to use to encrypt the data managed by the resources created by this module. Encryption at rest is not optional."
}

variable "dydb_read_cap" {
  description = "Optional read capacity for the Dynamo DB table associated with this module. Defaults to 5."
  default     = 5
}

variable "dydb_write_cap" {
  description = "Optional write capacity for the Dynamo DB table associated with this module. Defaults to 5."
  default     = 5
}

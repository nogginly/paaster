resource "aws_iam_user" "tfbe_user" {
  name          = "tfbe_${var.name}_${var.user_suffix}"
  force_destroy = true
}

// Load up the GPG public key file needed to properly setup IAM users
data "local_file" "b64publickey" {
  filename = "${var.gpg_publickey_file}"
}

resource "aws_iam_access_key" "tfbe_user" {
  user    = "${aws_iam_user.tfbe_user.name}"
  pgp_key = "${data.local_file.b64publickey.content}"
}

resource "aws_iam_user_policy_attachment" "tfbe_user" {
  user       = "${aws_iam_user.tfbe_user.name}"
  policy_arn = "${var.tfbe_policy_arn}"
}

resource "aws_iam_user_policy" "tfbe_user_allow_kms" {
  name = "kms"
  user = "${aws_iam_user.tfbe_user.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [ 
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:Describe*"
      ],
      "Resource": "${var.key_arn}"
    },
    {
      "Effect": "Allow",
      "Action": [ 
        "kms:GenerateDataKey*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_user_policy" "tfbe_user_allow_other" {
  name = "other"
  user = "${aws_iam_user.tfbe_user.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [ 
        "ec2:DescribeAccountAttributes",
        "iam:GetUser"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

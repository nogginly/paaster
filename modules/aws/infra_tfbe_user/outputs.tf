output "tfbe_user_accesskey" {
  description = "The AWS API access key for the TFBE user, to be used when configuring remote backend for application config using terraform."
  value       = "${aws_iam_access_key.tfbe_user.id}"
}

output "tfbe_user_deploy_enc_secretkey" {
  description = "The GPG-encrypted base64-encoded AWS API secret key for the TFBE user, to be used when configuring remote backend for application config using terraform."
  value       = "${aws_iam_access_key.tfbe_user.encrypted_secret}"
}

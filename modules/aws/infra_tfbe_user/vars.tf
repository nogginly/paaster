variable "name" {
  description = "A short nameto be used as prefix where possible for resources created by this module. Keep it short and use following pattern: " # [a-z\]+\[a-z0-9\\-\]+`"
}

variable "key_arn" {
  description = "ARN for the KMS key to allow the user to use when accessing encrypted resources."
}

variable "user_suffix" {
  description = "An optional short name to be used as suffix to the IAM user created by this module. Default is \"user\"."
  default     = "user"
}

variable "gpg_publickey_file" {
  description = "Path to the base64 encoded GPG public key file used to encrypt the AWS access and secret key for the deployment user."
}

variable "tfbe_policy_arn" {
  description = "The IAM policy ARN that allows this user to access/use the TFBE resources."
}
